<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211206160009 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE language_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE student_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE students_class_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE teacher_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE language (id INT NOT NULL, language VARCHAR(255) NOT NULL, short VARCHAR(3) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4DB71B5D4DB71B5 ON language (language)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4DB71B58F2890A2 ON language (short)');
        $this->addSql('CREATE TABLE student (id INT NOT NULL, students_class_id INT NOT NULL, language_group_id INT NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, gender BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B723AF33F6CF6CAE ON student (students_class_id)');
        $this->addSql('CREATE INDEX IDX_B723AF3392F6817D ON student (language_group_id)');
        $this->addSql('CREATE TABLE students_class (id INT NOT NULL, main_teacher_id INT NOT NULL, number INT NOT NULL, char VARCHAR(2) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E1587E6B79780A7E ON students_class (main_teacher_id)');
        $this->addSql('CREATE UNIQUE INDEX class_number_char_unique ON students_class (number, char)');
        $this->addSql('CREATE TABLE teacher (id INT NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, gender BOOLEAN NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF33F6CF6CAE FOREIGN KEY (students_class_id) REFERENCES students_class (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF3392F6817D FOREIGN KEY (language_group_id) REFERENCES language (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE students_class ADD CONSTRAINT FK_E1587E6B79780A7E FOREIGN KEY (main_teacher_id) REFERENCES teacher (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE student DROP CONSTRAINT FK_B723AF3392F6817D');
        $this->addSql('ALTER TABLE student DROP CONSTRAINT FK_B723AF33F6CF6CAE');
        $this->addSql('ALTER TABLE students_class DROP CONSTRAINT FK_E1587E6B79780A7E');
        $this->addSql('DROP SEQUENCE language_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE student_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE students_class_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE teacher_id_seq CASCADE');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE student');
        $this->addSql('DROP TABLE students_class');
        $this->addSql('DROP TABLE teacher');
    }
}
