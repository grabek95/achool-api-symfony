<?php

namespace App\Tests\Services;

use App\Entity\Teacher;
use App\Repository\TeacherRepository;
use App\Services\ApiServices\ApiTeachersService;

class ApiTeacherServiceTest extends ApiServiceTest
{
    private ApiTeachersService $apiTeacherServiceTest;

    public function setUp(): void
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $this->teacherRepository = $kernel->getContainer()->get(TeacherRepository::class);
        $this->apiTeacherServiceTest = new ApiTeachersService($this->teacherRepository);
    }

    public function testGetTeachersWithClass()
    {

        $teachersResult = $this->apiTeacherServiceTest->getTeachersWithClasses();
        $this->assertCount(2, $teachersResult);
        foreach ($teachersResult as $teacher) {
            self::assertNotNull($teacher['students_class']);
        }
        $allTeachers = $this->entityManager->getRepository(Teacher::class)->findAll();
        $this->assertCount(3, $allTeachers);
    }
}
