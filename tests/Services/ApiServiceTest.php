<?php

namespace App\Tests\Services;

use App\Entity\Language;
use App\Entity\Student;
use App\Entity\StudentsClass;
use App\Entity\Teacher;
use DateTime;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class ApiServiceTest extends KernelTestCase
{

    protected $entityManager;
    private $faker;
    private $languages;
    private $teachers;
    private $studentsClasses;


    protected function setUp(): void
    {
        parent::setUp();
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->truncateAllEntities();
        $this->loadData();
    }

    protected function loadData()
    {
        $this->faker = Factory::create();
        $this->loadLanguages();
        $this->loadTeachers();
        $this->loadStudentsClasses();
        $this->loadStudents();
        $this->entityManager->flush();

    }

    private function loadLanguages()
    {
        $langPol = new Language();
        $langPol->setLanguage('Polski');
        $langPol->setShort('POL');
        $langPol->setCreatedAt(new DateTime());
        $this->entityManager->persist($langPol);

        $langEng = new Language();
        $langEng->setLanguage('Angielski');
        $langEng->setShort('ENG');
        $langEng->setCreatedAt(new DateTime());
        $this->entityManager->persist($langEng);
        $this->languages[] = $langPol;
        $this->languages[] = $langEng;
    }

    private function loadTeachers()
    {
        $teacher = new Teacher();
        $teacher->setName('Jan');
        $teacher->setSurname('Kowalski');
        $teacher->setGender(0);
        $teacher->setCreatedAt(new DateTime());
        $this->entityManager->persist($teacher);
        $this->teachers[] = $teacher;

        $teacher2 = new Teacher();
        $teacher2->setName('Janina');
        $teacher2->setSurname('Kowalska');
        $teacher2->setGender(1);
        $teacher2->setCreatedAt(new DateTime());
        $this->entityManager->persist($teacher2);
        $this->teachers[] = $teacher2;

        $teacher3 = new Teacher();
        $teacher3->setName('Janina');
        $teacher3->setSurname('Kowalska');
        $teacher3->setGender(1);
        $teacher3->setCreatedAt(new DateTime());
        $this->entityManager->persist($teacher3);
        $this->teachers[] = $teacher3;

    }

    private function loadStudentsClasses()
    {
        $studentsClass = new StudentsClass();
        $studentsClass->setNumber(6);
        $studentsClass->setChar('C');
        $studentsClass->setMainTeacher($this->teachers[0]);
        $studentsClass->setCreatedAt(new DateTime());
        $this->entityManager->persist($studentsClass);
        $this->studentsClasses[] = $studentsClass;

        $studentsClass2 = new StudentsClass();
        $studentsClass2->setNumber(6);
        $studentsClass2->setChar('B');
        $studentsClass2->setMainTeacher($this->teachers[1]);
        $studentsClass2->setCreatedAt(new DateTime());
        $this->entityManager->persist($studentsClass2);
        $this->studentsClasses[] = $studentsClass2;
    }

    private function loadStudents()
    {
        for ($i = 0; $i < 4; $i++) {
            $student = new Student();
            $student->setName($this->faker->firstName);
            $student->setSurname($this->faker->lastName);
            $student->setGender($this->faker->boolean(50));
            $student->setLanguageGroup($this->languages[$i % 2]);
            $student->setStudentsClass($this->studentsClasses[0]);
            $student->setCreatedAt(new DateTime());
            $this->entityManager->persist($student);
        }
        for ($i = 0; $i < 4; $i++) {
            $student = new Student();
            $student->setName($this->faker->firstName);
            $student->setSurname($this->faker->lastName);
            $student->setGender($this->faker->boolean(50));
            $student->setLanguageGroup($this->languages[1]);
            $student->setStudentsClass($this->studentsClasses[1]);
            $student->setCreatedAt(new DateTime());
            $this->entityManager->persist($student);
        }
    }

    protected function truncateEntities(array $entities)
    {
        $connection = $this->entityManager->getConnection();
        $databasePlatform = $connection->getDatabasePlatform();
        foreach ($entities as $entity) {
            $query = $databasePlatform->getTruncateTableSQL(
                $this->entityManager->getClassMetadata($entity)->getTableName(), true
            );
            $connection->executeUpdate($query);
        }
    }

    protected function truncateAllEntities()
    {
        $this->truncateEntities([
            Language::class,
            Teacher::class,
            Student::class,
            StudentsClass::class,
        ]);
    }

}
