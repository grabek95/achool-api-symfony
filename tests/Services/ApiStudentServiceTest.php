<?php

namespace App\Tests\Services;

use App\Repository\StudentRepository;
use App\Services\ApiServices\ApiStudentsService;

class ApiStudentServiceTest extends ApiServiceTest
{
    private ApiStudentsService $apiStudentService;

    public function setUp(): void
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $this->studentRepository = $kernel->getContainer()->get(StudentRepository::class);
        $this->apiStudentService = new ApiStudentsService($this->studentRepository);
    }

    public function testGetStudentsByClass()
    {
        $number = 6;
        $char = 'C';
        $studentsResults = $this->apiStudentService->getStudentsByClassAndMapToArray($number, $char);
        $this->assertCount(4, $studentsResults);
        foreach ($studentsResults as $student) {
            self::assertEquals($number, $student['students_class']['number']);
            self::assertEquals($char, $student['students_class']['char']);
        }
    }

    public function testGetStudentsByClassAndLanguage()
    {

        $number = 6;
        $char = 'C';
        $lang = 'ENG';
        $studentsResults = $this->apiStudentService->getStudentsByClassAndLangAndMapToArray($number, $char, $lang);
        $this->assertCount(2, $studentsResults);
        foreach ($studentsResults as $student) {
            self::assertEquals($number, $student['students_class']['number']);
            self::assertEquals($char, $student['students_class']['char']);
            self::assertEquals($lang, $student['lang']['short']);
        }
    }

}
