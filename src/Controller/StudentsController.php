<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StudentsController extends AbstractController
{
    /**
     * @Route("/studentsByClass", name="students_by_class")
     */
    public function studentsByClassClass(): Response
    {
        return $this->render('students/students_by_class.html.twig', [
            'controller_name' => 'Uczniwie - dane pobrane z API',
        ]);
    }

    /**
     * @Route("/studentsByClassAndLang", name="students_by_class_and_lang")
     */
    public function studentsByClassClassAndLang(): Response
    {
        return $this->render('students/students_by_class_and_lang.html.twig', [
            'controller_name' => 'Uczniwie - dane pobrane z API',
        ]);
    }
}
