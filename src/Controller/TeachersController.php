<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeachersController extends AbstractController
{
    /**
     * @Route("/teachersWithClasses", name="teachersWithclasses")
     */
    public function teachersWithClasses(): Response
    {
        return $this->render('teachers/teachers_with_classes.html.twig', [
            'controller_name' => 'Nauczyciele bedący wychowawcami - dane pobrane z API',
        ]);
    }
}
