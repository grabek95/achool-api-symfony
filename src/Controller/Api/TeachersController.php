<?php

namespace App\Controller\Api;

use App\Services\ApiServices\ApiTeachersService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeachersController extends AbstractController
{
    private $apiTeachersService;

    public function __construct(ApiTeachersService $apiTeachersService)
    {
        $this->apiTeachersService = $apiTeachersService;
    }

    /**
     * @Route("/api/teachersWithClasses", name="teachersWithclasses_api", methods={"GET"})
     */
    public function teachersWithClasses(): Response
    {
        $result = $this->apiTeachersService->getTeachersWithClasses();
        return new JsonResponse($result, Response::HTTP_OK);
    }
}
