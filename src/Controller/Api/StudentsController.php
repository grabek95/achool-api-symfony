<?php

namespace App\Controller\Api;

use App\Services\ApiServices\ApiStudentsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StudentsController extends AbstractController
{
    private $apiStudentService;

    public function __construct(ApiStudentsService $apiStudentsService)
    {
        $this->apiStudentService = $apiStudentsService;
    }

    /**
     * @Route("/api/studentsByClass/{number}/{char}", name="api_students_by_class", methods={"GET"})
     */
    public function studentsByClass($number, $char): Response
    {
        $result = $this->apiStudentService->getStudentsByClassAndMapToArray($number, strtoupper($char));
        return new JsonResponse($result, Response::HTTP_OK);

    }

    /**
     * @Route("/api/studentsByClassAndLang/{number}/{char}/{language}", name="api_students_by_class_and_lng", methods={"GET"})
     */
    public function studentsByClassAndLang($number, $char, $language): Response
    {
        $result = $this->apiStudentService->getStudentsByClassAndLangAndMapToArray($number, strtoupper($char), strtoupper($language));

        return new JsonResponse($result, Response::HTTP_OK);
    }
}
