<?php

namespace App\Entity;

use App\Repository\StudentsClassRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Table(
 *     name="students_class",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="class_number_char_unique", columns={"number", "char"})
 *     }
 * )
 * @ORM\Entity(repositoryClass=StudentsClassRepository::class)
 */
class StudentsClass
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $char;


    /**
     * @ORM\OneToOne(targetEntity=Teacher::class, inversedBy="students_class", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $main_teacher;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity=Student::class, mappedBy="students_class")
     */
    private $students;

    public function __construct()
    {
        $this->students = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getChar(): ?string
    {
        return $this->char;
    }

    public function setChar(string $char): self
    {
        $this->char = strtoupper($char);

        return $this;
    }

    public function getMainTeacher(): ?Teacher
    {
        return $this->main_teacher;
    }

    public function setMainTeacher(Teacher $main_teacher): self
    {
        $this->main_teacher = $main_teacher;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(DateTime $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setStudentsClass($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getStudentsClass() === $this) {
                $student->setStudentsClass(null);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'number' => $this->getNumber(),
            'char' => $this->getChar(),
            'main_teacher' => [
                'id' => $this->getMainTeacher()->getId(),
                'name' => $this->getMainTeacher()->getName(),
                'surname' => $this->getMainTeacher()->getSurname(),
            ],
            'crated_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];
    }
}
