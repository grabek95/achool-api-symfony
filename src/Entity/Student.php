<?php

namespace App\Entity;

use App\Repository\StudentRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StudentRepository::class)
 */
class Student
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="boolean")
     */
    private $gender;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity=StudentsClass::class, inversedBy="students")
     * @ORM\JoinColumn(nullable=false)
     */
    private $students_class;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class, inversedBy="students")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language_group;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getGender(): ?bool
    {
        return $this->gender;
    }

    public function setGender(bool $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(DateTime $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getStudentsClass(): ?StudentsClass
    {
        return $this->students_class;
    }

    public function setStudentsClass(?StudentsClass $students_class): self
    {
        $this->students_class = $students_class;

        return $this;
    }

    public function getLanguageGroup(): ?Language
    {
        return $this->language_group;
    }

    public function setLanguageGroup(?Language $language_group): self
    {
        $this->language_group = $language_group;

        return $this;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'surname' => $this->getSurname(),
            'gender' => $this->getGender(),
            'lang' => [
                'id' => $this->getLanguageGroup()->getId(),
                'language' => $this->getLanguageGroup()->getLanguage(),
                'short' => $this->getLanguageGroup()->getShort(),
            ],
            'students_class' => [
                'id' => $this->getStudentsClass()->getId(),
                'number' => $this->getStudentsClass()->getNumber(),
                'char' => $this->getStudentsClass()->getChar(),
            ],
            'crated_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];
    }
}
