<?php

namespace App\Services\ApiServices;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class ApiService
{
    protected ServiceEntityRepository $serviceEntityRepository;

    public function __construct(ServiceEntityRepository $serviceEntityRepository)
    {
        $this->serviceEntityRepository = $serviceEntityRepository;
    }

    /**
     * @param Object[] $objects
     * @return void
     */
    protected function mapObjectsToArray(array $objects)
    {
        return array_map(function ($object){return $object->toArray();},$objects );
    }

}