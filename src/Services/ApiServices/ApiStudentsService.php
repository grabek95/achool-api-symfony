<?php

namespace App\Services\ApiServices;

use App\Repository\StudentRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class ApiStudentsService extends ApiService
{
    public function __construct(StudentRepository $studentRepository)
    {
        parent::__construct($studentRepository);
    }

    public function getStudentsByClassAndMapToArray($number, $char)
    {
        $result = $this->serviceEntityRepository->getStudentsByClassOrderByGender($number, $char);

        return $this->mapObjectsToArray($result);
    }

    public function getStudentsByClassAndLangAndMapToArray($number, $char, $language)
    {
        $result = $this->serviceEntityRepository->getStudentsByClassAndLang($number, $char, $language);

        return $this->mapObjectsToArray($result);
    }

}