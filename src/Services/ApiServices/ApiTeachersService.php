<?php

namespace App\Services\ApiServices;

use App\Repository\TeacherRepository;

class ApiTeachersService extends ApiService
{
    public function __construct(TeacherRepository $teacherRepository)
    {
        parent::__construct($teacherRepository);
    }

    public function getTeachersWithClasses()
    {
        $result = $this->serviceEntityRepository->getTeachersWithClass();

        return $this->mapObjectsToArray($result);
    }

}