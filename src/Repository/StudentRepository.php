<?php

namespace App\Repository;

use App\Entity\Language;
use App\Entity\Student;
use App\Entity\StudentsClass;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Student|null find($id, $lockMode = null, $lockVersion = null)
 * @method Student|null findOneBy(array $criteria, array $orderBy = null)
 * @method Student[]    findAll()
 * @method Student[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct
    (
        ManagerRegistry         $registry,
        EntityManagerInterface  $manager
    )
    {
        parent::__construct($registry, Student::class);
        $this->manager = $manager;
    }

    public function getStudentsByClassOrderByGender($classNumber, $classChar)
    {
        return $this->getStudentsByClassQueryBuilder($classNumber, $classChar)
            ->orderBy('s.gender', 'ASC')
            ->setParameters([
                'number' => $classNumber,
                'char' => $classChar,
            ])
            ->getQuery()
            ->getResult();
    }
    public function getStudentsByClassAndLang($classNumber, $classChar, $language)
    {
        return $this->getStudentsByClassQueryBuilder($classNumber, $classChar)
            ->innerJoin(Language::class, 'lng', Join::WITH, 's.language_group = lng.id')
            ->andWhere('lng.short = :language')
            ->orWhere('lng.language = :language')
            ->setParameters([
                'number' => $classNumber,
                'char' => $classChar,
                'language' => $language
            ])
            ->getQuery()
            ->getResult();
    }
    private function getStudentsByClassQueryBuilder($classNumber, $classChar){
        return $this->createQueryBuilder('s')
            ->innerJoin(StudentsClass::class, 'sc', Join::WITH, 's.students_class = sc.id')
            ->andWhere('sc.number = :number')
            ->andWhere('sc.char = :char');
    }

    // /**
    //  * @return Student[] Returns an array of Student objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Student
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
