<?php

namespace App\DataFixtures;

use App\Entity\Language;
use App\Entity\Student;
use App\Entity\StudentsClass;
use App\Entity\Teacher;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    private $manager;
    private $faker;
    private $languages;
    private $teachers;
    private $studentsClasses;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->faker = Factory::create();
        $this->loadLanguages();
        $this->loadTeachers();
        $this->loadStudentsClasses();
        $this->loadStudents();
        $this->manager->flush();

    }

    private function loadLanguages()
    {
        $langPol = new Language();
        $langPol->setLanguage('Polski');
        $langPol->setShort('POL');
        $langPol->setCreatedAt(new DateTime());
        $this->manager->persist($langPol);

        $langEng = new Language();
        $langEng->setLanguage('Angielski');
        $langEng->setShort('ENG');
        $langEng->setCreatedAt(new DateTime());
        $this->manager->persist($langEng);
        $this->languages[] = $langPol;
        $this->languages[] = $langEng;
    }

    private function loadTeachers()
    {
        for ($i = 0; $i < 12; $i++) {
            $teacher = new Teacher();
            $teacher->setName($this->faker->firstName);
            $teacher->setSurname($this->faker->lastName);
            $teacher->setGender($this->faker->boolean(50));
            $teacher->setCreatedAt(new DateTime());
            $this->manager->persist($teacher);
            $this->teachers[] = $teacher;
        }
    }

    private function loadStudentsClasses()
    {
        $classes = [
            ['number' => 1, 'char' => 'A'],
            ['number' => 1, 'char' => 'B'],
            ['number' => 1, 'char' => 'C'],
            ['number' => 2, 'char' => 'A'],
            ['number' => 2, 'char' => 'B'],
            ['number' => 2, 'char' => 'C'],
            ['number' => 3, 'char' => 'A'],
            ['number' => 3, 'char' => 'B'],
            ['number' => 3, 'char' => 'C'],
            ['number' => 4, 'char' => 'A'],
            ['number' => 4, 'char' => 'B'],
            ['number' => 4, 'char' => 'C'],
            ['number' => 5, 'char' => 'A'],
            ['number' => 5, 'char' => 'B'],
            ['number' => 5, 'char' => 'C'],
            ['number' => 6, 'char' => 'A'],
            ['number' => 6, 'char' => 'B'],
        ];
        $studentsClass = new StudentsClass();
        $studentsClass->setNumber(6);
        $studentsClass->setChar('C');
        $indexTeacher = array_rand($this->teachers, 1);
        $studentsClass->setMainTeacher($this->teachers[$indexTeacher]);
        unset($this->teachers[$indexTeacher]);
        $studentsClass->setCreatedAt(new DateTime());
        $this->manager->persist($studentsClass);
        $this->studentsClasses[] = $studentsClass;
        for ($i = 0; $i < 6; $i++) {
            $studentsClass = new StudentsClass();
            $indexClasses = array_rand($classes, 1);
            $studentsClass->setNumber($classes[$indexClasses]['number']);
            $studentsClass->setChar($classes[$indexClasses]['char']);
            unset($classes[$indexClasses]);
            $indexTeacher = array_rand($this->teachers, 1);
            $studentsClass->setMainTeacher($this->teachers[$indexTeacher]);
            unset($this->teachers[$indexTeacher]);
            $studentsClass->setCreatedAt(new DateTime());
            $this->manager->persist($studentsClass);
            $this->studentsClasses[] = $studentsClass;
        }
    }

    private function loadStudents()
    {
        for ($i = 0; $i < 70; $i++) {
            $student = new Student();
            $student->setName($this->faker->firstName);
            $student->setSurname($this->faker->lastName);
            $student->setGender($this->faker->boolean(50));
            $student->setLanguageGroup($this->faker->randomElement($this->languages));
            $student->setStudentsClass($this->faker->randomElement($this->studentsClasses));
            $student->setCreatedAt(new DateTime());
            $this->manager->persist($student);
        }
    }

}
