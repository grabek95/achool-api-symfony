## Instrukcja uruchomienia projektu

Aby uruchomic project należy:

- posiadac zainstalowanego dockera i docker-compose
- wpliku .env ustawione są porty zewnetrzne dla aplikacji domyslnie aplikacja jest postawiona na porcie 8000, z którego
  pobierane sa dane w ajaxie w przykladowym wyswietleniu danych za pomocą ajaxa
- odpalic skrypt 'sh docker.sh' (uzyte porty w apikacji 8000,5431,5430)
- przy kolejnych odpaleniach dockerow mozna zakomentowac migracje baz
- na koncu wykonaja się testy
- dataFixtures stworza przykladowa baze, na pewno bedzie istniec klasa 6C do testów
- dodane języki to Polski ze skrótem pol i angielski ze skrótem eng. do wysukiwania uczniów z klasy pozna użyc
  wszystkich wersji bez czułości na wielkość liter

Można podłączyc sie do bazy dockerowej pod adresem lokalny na portach:

- 5431 baza główna
- 5430 baza testowa
- user : symfony pass : ChangeMe
- baza główna app, testowa app_test

Dane z API beda dostępne pod adresami :

- http://localhost:8000/api/studentsByClass/{number}/{char}
- http://localhost:8000/api/studentsByClassAndLang/{number}/{char}/{language}
- http://localhost:8000/api/teachersWithClasses

Dane pobrane z API są wyswietlane w widokach pod adresami

- http://localhost:8000/studentsByClass
- http://localhost:8000/studentsByClassAndLang
- http://localhost:8000/teachersWithClasses