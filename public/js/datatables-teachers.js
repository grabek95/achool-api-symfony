function getAndLoadTeachers(url) {
    var table = $('#table').DataTable();
    table.destroy();

    $('#table').DataTable({
        ajax: {
            url: url,
            method: "GET",
            xhrFields: {
                withCredentials: true
            },
            dataSrc: ""
        },
        columns: [
            {data: "id"},
            {data: "name"},
            {data: "surname"},
            {
                data: "gender", "render": function (data, type, row) {
                    return data ? 'M' : 'K';
                }
            },
            {
                data: "students_class", "render": function (data, type, row) {
                    return data.number + data.char;
                }
            },
        ],
        order: []
    });

}

function showTeachersWithClass() {
    let url = 'http://localhost:8000/api/teachersWithClasses';
    getAndLoadTeachers(url)
}

$(document).ready(function () {
    showTeachersWithClass()
});
