function getAndLoadStudents(url) {
    var table = $('#table');
    table.DataTable().destroy();

    table.DataTable({
        ajax: {
            url: url,
            method: "GET",
            xhrFields: {
                withCredentials: true
            },
            dataSrc: ""
        },
        columns: [
            {data: "id"},
            {data: "name"},
            {data: "surname"},
            {
                data: "gender", "render": function (data, type, row) {
                    return data ? 'M' : 'K';
                }
            },
            {data: "lang.language"},
        ],
        order: []
    });

}

$(document).ready(function () {

    $(document).on('click', '.show-students-by-class', function () {
            let number = $('#number').val();
            let char = $('#char').val();
            let url = 'http://localhost:8000/api/studentsByClass/' + number + '/' + char;
            if (number != '' && char != '') {
                getAndLoadStudents(url)
            } else {
                alert('Brak wprowadzonych danych!')
            }
        }
    )

    $(document).on('click', '.show-students-by-lang', function () {
            let number = $('#number').val();
            let char = $('#char').val();
            let lang = $('#lang').val();
            let url = 'http://localhost:8000/api/studentsByClassAndLang/' + number + '/' + char + '/' + lang;
            if (number != '' && char != '') {
                getAndLoadStudents(url)
            } else {
                alert('Brak wprowadzonych danych!')
            }
        }
    )
});
