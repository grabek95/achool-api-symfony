#docker rm $(docker ps -a -q) -f #removes your containers
#docker network prune -f #remove all unused networks
docker-compose up -d #running your containers
docker exec -it school-app composer install
docker exec -it school-app php bin/console --no-interaction doctrine:migrations:migrate
docker exec -it school-app php bin/console --no-interaction doctrine:migrations:migrate --env=test
docker exec -it school-app php bin/console --no-interaction doctrine:fixtures:load
docker exec -it school-app php bin/console cache:clear
docker exec -it school-app php ./vendor/bin/phpunit

